const path = require('path');
const webpack = require('webpack');

const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = (envVars, dirname) => ({
  mode: 'production',
  entry: {
    main: ['index.js']
  },
  output: {
    path: path.join(dirname, '/build'),
    filename: '[name].[hash].bundle.js',
    publicPath: '/'
  },
  plugins: [
    new webpack.EnvironmentPlugin({ ...envVars }),
    new OptimizeCSSAssetsPlugin()
  ],
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true
      }),
      new OptimizeCSSAssetsPlugin()
    ]
  }
});