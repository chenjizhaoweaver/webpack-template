# webpack-template

## Introduction
A well managed webpack template for quick initiation of a react project

## Major Concern
* Quick kickoff of a react project
* Test environment setup
* Prepare compile configurations for javascript, css and html
* Enable direct import of json in project
* Csv could be imported as text file in project
* Balance between (1) production build performance; (2) consistency between releases; (3) Simplicity