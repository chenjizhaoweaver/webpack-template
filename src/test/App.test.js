import React from 'react';
import { mount } from 'enzyme';

import App from '../app/App';

test('dummy test', () => {
  const wrapper = mount(<App />);
  expect(wrapper).toBeDefined();
});